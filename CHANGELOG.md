# Changes in version 1.0.0 - 2023-06-08
  * Medium changes:
    - Parse main Tor network documents
    - Add data to a postgresqlDB
    - Send time serires data to VictoriaMetrics
