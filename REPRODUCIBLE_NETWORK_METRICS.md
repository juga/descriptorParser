# Reproducible Network Metrics

The graphs and tables you see on Tor Metrics are the result of combining data from various points within the Tor network. Some of these combinations are straightforward, while others are more complex.

Our aim is to make the graphs and tables produced by our metrics services more accessible and reproducible. On this page, we provide comprehensive instructions on how you can replicate the data behind these graphs and tables using the timeseries data published on VictoriaMetrics. This enables you to create your own versions. Additionally, we offer insights into the design principles guiding our data aggregations and provide links to technical reports and additional resources.

This page is a living document that keeps up with the latest changes in the graphs and tables on Tor Metrics.

## Users

One of the most crucial statistics we monitor is the number of Tor users. Understanding the daily usage of the Tor network, including how users connect through relays or bridges, their geographic origins, preferred transport methods, and whether they use IPv4 or IPv6, is essential.

However, due to the inherent privacy and anonymity aspects of Tor, we cannot gather identifiable information to determine the exact number of users. Therefore, we do not directly count users but rather track requests made by clients to the directories or bridges. We use this data to indirectly estimate the average number of concurrent users throughout the day.

In this context, we are unable to distinguish whether the same group of users remains connected throughout the entire day or if there is a turnover of users over shorter intervals. Our primary goal is to detect changes in usage patterns, making it unnecessary to provide precise absolute user numbers.

### Estimating Relay User Statistics

Relay users are individuals who directly connect to a relay to access the Tor network, as opposed to bridge users who use a bridge as their entry point. The steps involved in estimating relay user statistics are quite similar to those used for calculating bridge user statistics, which are explained in more detail below.

#### Step 1: Parsing Consensuses to Identify Active Relays

Obtain consensuses from CollecTor. For details on the descriptor format, refer to the Tor directory protocol, version 3.
From each consensus, extract the "valid-after" and "fresh-until" times from the header section.
In each consensus entry, decode the base64-encoded relay fingerprint from the "r" line and extract the relay flags from the "s" line. If there is no "Running" flag, skip this entry. (Note that consensuses with consensus method 4, introduced in 2008 or later, do not list non-running relays, so checking relay flags in recent consensuses is largely a precaution with minimal impact on the parsed data.)

#### Step 2: Parsing Relay Extra-Info Descriptors for Relevant Statistics

Also, obtain relay extra-info descriptors from CollecTor. As mentioned earlier, refer to the Tor directory protocol, version 3, for descriptor format details.
Parse the relay fingerprint from the "extra-info" line and the descriptor publication time from the "published" line.
Analyze the "dirreq-write-history" line, which contains data on bytes spent in responding to directory requests. If the statistics end time in this line is more than one week older than the descriptor's publication time, exclude this data to avoid redundancy, as it was likely reported in earlier descriptors and already processed. If a statistics interval spans more than one UTC date, distribute observations linearly across those dates.
Parse data from the "dirreq-stats-end," "dirreq-v3-resp," and "dirreq-v3-reqs" lines, which contain directory-request statistics. Exclude directory request statistics if the statistics end time is more than one week older than the descriptor publication time or if the statistics interval is not one day. For successful requests, adjust by subtracting 4 to reverse the binning operation applied by the relay, and discard the result if it's zero or negative. Similarly, for successful requests by country, subtract 4 from each number to undo the binning operation and discard if the result is zero or negative. As before, split observations across covered UTC dates by assuming a linear distribution.

#### Step 3: Approximate directory requests by country

Relays provide data on directory requests in two ways: as a total number (in the "dirreq-v3-resp" line) and as numbers categorized by country (in the "dirreq-v3-reqs" line). Instead of using the country-specific numbers directly, we calculate the approximate directory requests from each country by multiplying the total requests by the fraction of requests originating from that specific country. This approach serves two purposes: it minimizes the overall impact of binning, and it ensures that relay and bridge user estimates are more consistent. If a relay, for any reason, only reports the total requests and not requests by country, we attribute all those requests to an "Unknown Country" represented as "??" in our calculations.

#### Step 4: Estimating the Fraction of Reported Directory-Request Statistics

Following the parsing of descriptors, the next step is to estimate the fraction of reported directory-request statistics for a given day. This fraction is crucial for extrapolating observed request numbers to expected network totals. For further context and detailed calculations, please refer to the technical report titled "Counting daily bridge users," which is also applicable to relay users. In the following explanation, we use the term "server" instead of "relay" or "bridge" because the estimation method remains the same for both.

For each day in the specified time period, calculate five variables:

Compute n(N): This represents the total server uptime in hours on a given day, which is the sum of all server uptime hours for that day. It's calculated by adding up the intervals between "valid-after" and "fresh-until" for all consensuses with a valid-after time on that day. A more intuitive interpretation of this variable is the average number of running servers. However, this interpretation only holds if fresh consensuses are available for all hours of a day.

Compute n(H): This is the total number of hours for which servers have reported written directory bytes on a given day.

Compute n(R\H): This represents the number of hours during which responses have been reported, but no written directory bytes. To calculate this fraction, add up all interval lengths and then subtract the interval length of written directory bytes from the interval length of directory responses. Negative results are disregarded.

Compute h(H): This denotes the total number of written directory bytes on a given day.

Compute h(R^H): This is the number of written directory bytes for the fraction of time when a server was reporting both written directory bytes and directory responses. To determine this fraction, first sum up all interval lengths and then compute the minimum of both sums divided by the sum of reported written directory bytes.

From these variables, calculate the estimated fraction of reported directory-request statistics using the following formula:

```
frac = (h(R^H) * n(H) + h(H) * n(R\H)) / (h(H) * n(N))
```

#### Step 5: Computing Estimated Relay Users per Country

With the estimated fraction of reported directory-request statistics obtained in the previous step, it is now possible to calculate estimates for relay users. The same approach applies to estimating bridge users by country, transport, or IP version, as described below.

First, calculate r(R) as the sum of reported successful directory requests from a specific country on a given day. This approach is also applicable if you want to calculate r(R) as the sum of requests from all countries or from any other subset of countries, depending on your specific interest.

Estimate the number of clients per country and per day using the following formula:


```
r(N) = floor(r(R) / frac / 10)
```

A client connected 24/7 makes approximately 15 requests per day, but not all clients are online constantly. Therefore, we use the number 10 as an average. We divide the directory requests by 10, considering the result as the number of users. In another perspective, we assume that each request represents a client online for one-tenth of a day, which is equivalent to 2 hours and 24 minutes.

Exclude dates where frac is less than 10% as this value is too low for a reliable estimate. Similarly, skip dates where frac exceeds 110%, indicating a potential issue in the previous step. We use 110% as the upper bound to allow for relays reporting statistics that may have temporarily missed inclusion in the consensus, accepting up to 10% additional statistics. However, an upper limit is necessary to exclude extreme outliers with fractions of 120%, 150%, or even 200%.

### Processing Relay User Statistics from VictoriaMetrics timeseries

The procedure described produces the `desc_relay_dirreq_v3_responses` metrics on
VictoriaMetrics.

The metric uses different labels that permit aggregating user statistics per country.

For example the query:

```
sum(sum_over_time(max(desc_relay_dirreq_v3_responses{country="$country"}) by (day, fingerprint))[24h]) by (day) / 10
```

To obtain the overall number of relay users you can run the query with `$country=""`

```
sum(sum_over_time(max(desc_relay_dirreq_v3_responses{country=""}) by (day, fingerprint))[24h]) by (day) / 10
```

Note that on VictoriaMetrics we do not compute fractions so, for now, the results
differs slightly from what is published on metrics.torproject.org

### Estimating Bridge User Statistics

Bridge users are individuals who utilize a bridge to access the Tor network, which serves as an entry point, unlike relay users who connect directly to a relay. Many of the procedures involved in estimating the number of bridge users are analogous to those detailed above for estimating the count of relay users.

Here are some ways to categorize bridge users:

    - Bridge users based on their country of origin.
    - Bridge users categorized by the type of transport they use.
    - A combination of both country and transport to classify bridge users.
    - Bridge users categorized by their IP version.
    - A list of the top 10 countries with the highest number of bridge users.

#### Step 1: Extract Information from Bridge Network Statuses to Identify Running Bridges

To begin, collect bridge network statuses using CollecTor.

For each status entry, extract the "published" timestamp from the header section.

From every status entry, decode the base64-hashed bridge fingerprint found in the "r" line. Additionally, extract the relay flags from the "s" line. If the "Running" flag is absent, move on to the next entry.

It's important to note that unlike relay consensuses, bridge network statuses do not include "valid-after" or "fresh-until" times in their headers. To streamline our processing approach, we treat the publication hour as the "valid-after" time, and one hour later as the "fresh-until" time. When processing multiple statuses published within the same hour, we combine the running bridges found in those statuses to determine the running bridges for that specific hour.

#### Step 2: Extract Useful Statistics from Bridge Extra-Info Descriptors

In this step, we will also gather bridge extra-info descriptors from CollecTor. As mentioned earlier, you can find details on the descriptor format on the Tor bridge descriptors page.

Extract the hashed bridge fingerprint from the "extra-info" line and note the descriptor's publication time from the "published" line.

Analyze the "dirreq-write-history" line, which contains data regarding bytes spent on responding to directory requests. If the time interval of this data is over one week older than the publication time of the descriptor in the "published" line, we skip this information to avoid redundant statistics in our aggregation. If a statistics interval spans multiple UTC dates, we divide the observations among the relevant dates, assuming a linear distribution.

Process data from the "dirreq-stats-end," "dirreq-v3-resp," and "dirreq-v3-reqs" lines, which provide statistics on directory requests. Similar to the previous step, if the statistics end time in the "dirreq-stats-end" line is over one week older than the publication time of the descriptor, we skip these statistics to prevent duplication. We also disregard statistics with an interval length other than one day.

For successful requests in the "dirreq-v3-resp" line, we extract the data, subtract 4 to reverse a binning operation applied by the bridge, and exclude the number if it's zero or negative. Similarly, for successful requests by country in the "dirreq-v3-reqs" line, we follow the same process. If the statistics cover multiple UTC dates, we distribute them linearly.

Lastly, we examine the "bridge-ips," "bridge-ip-versions," and "bridge-ip-transports" lines, which contain information about unique connecting IP addresses categorized by country, IP version, and transport. For each count of unique IP addresses, we subtract 4 (to reverse the binning operation) and exclude the result if it's zero or negative.

#### Step 3: Estimating Directory Requests by Country, Transport, and IP Version

For older bridges that don't provide directory requests by country but only total requests and unique IP address counts by country, we estimate directory requests by country. This estimation involves multiplying the total number of requests by the fraction of unique IP addresses from a specific country. For newer bridges that report directory requests by country, we still use the total requests as our starting point and then multiply it by the fraction of requests from each country. This method ensures consistency with the reporting of totals by country, transport, and IP version. When a bridge doesn't report directory requests by country or unique IP addresses by country, we attribute all requests to "??," representing an unknown country.

Bridges also don't provide information about directory requests by transport or IP version. To approximate these numbers, we multiply the total number of requests by the fraction of unique IP addresses by transport or IP version. If a bridge fails to report unique IP addresses by transport or IP version, we attribute all requests to the default onion-routing protocol or to IPv4, respectively.

As a special case, we estimate lower and upper bounds for directory requests by country and transport. This estimation is based on the fact that most bridges offer a limited number of transports. We combine unique IP address sets by country and by transport to determine these bounds:

We calculate the lower bound as max(0, C(b) + T(b) - S(b), where C(b) represents the number of requests from a given country reported by bridge b, T(b) is the number of requests using a given transport reported by bridge b, and S(b) is the total number of requests reported by bridge b. The reasoning is that if the sum of C(b) + T(b) exceeds the total number of requests from all countries and transports (S(b)), there must be requests from that specific country and transport. If that is not the case, the lower limit is set to 0.

The upper bound is calculated as min(C(b), T(b), using the same definitions. The reasoning is that there cannot be more requests by country and transport than there are requests based on either of these two numbers.

#### Step 4: Estimating the Fraction of Reported Directory-Request Statistics

The process of estimating the fraction of reported directory-request statistics is quite similar for both bridges and relays. For the details of this estimation, please refer to Step 4 in the Relay users description.

Step 5: Computing Estimated Bridge Users by Country, Transport, or IP Version

This step is essentially the same for both bridge users and relay users. For the transformation of directory request numbers into user numbers, please refer to Step 5 in the Relay users description.

### Processing Bridge User Statistics from VictoriaMetrics timeseries

The procedure described produces the `desc_bridge_dirreq_v3_responses` metrics on
VictoriaMetrics.

The metric uses different labels that permit aggregating user statistics per
country, transport, ip version.

To calculate bridge users by transport you can run the query:

```
sum(sum_over_time(max(desc_bridge_dirreq_v3_responses{transport="obfs4"}) by (day, fingerprint))[24h]) by (day) / 10
```

To calculate total bridge users per `$country`:

```
sum(sum_over_time(max(desc_bridge_dirreq_v3_responses{country="$country"}) by (day, fingerprint))[24h]) by (day) / 10
```

Per `$country` and transport:

```
sum(
	sum_over_time
	(
		sum (
				max(desc_bridge_dirreq_v3_responses{transport="obfs4"}) by (fingerprint, day)
				+ on (fingerprint) max(desc_bridge_dirreq_v3_responses{country="$country"}) by (fingerprint, day)
				- on (fingerprint) max(desc_bridge_dirreq_v3_responses{}) by (fingerprint, day)
		) by (fingerprint, day) > 0
	)[24h]
)  by (day) / 10
```

To calculate total number of bridge users:

```
sum(
  sum_over_time(
    max(desc_bridge_dirreq_v3_responses{country=""}) by (day, fingerprint) +
    on (fingerprint) max(desc_bridge_dirreq_v3_responses{country!=""}) by (day, fingerprint)
  )[24h]
) by (day) / 10
```

## Server Statistics

Statistics regarding the quantity of servers, including relays and bridges, were among the initial metrics introduced on Tor Metrics. The majority of these statistics share a common characteristic: they rely on the count of running servers as their primary metric. While there are potential alternatives, such as using consensus weight totals or fractions, or guard/middle/exit probabilities as metrics, these alternatives have only recently been incorporated. In the following sections, we will outline the precise methods used for counting servers.


### Estimating Running Relays

We begin by delving into statistics concerning the quantity of running relays within the network. We break down this data according to various criteria, including relay flags, self-reported Tor version and operating system, and IPv6 capabilities. The descriptions provided here pertain to the following graphs:

  -  Relays and bridges (focusing on the relays section; details about bridges can be found below)
  -  Relays categorized by relay flags
  -  Relays categorized by Tor version
  -  Relays categorized by platform
  -  Relays categorized by IP version

#### Step 1: Parsing Consensuses

First, we gather consensuses from CollecTor, and for more information on the descriptor format, you can refer to the Tor directory protocol, version 3.

Extract and remember the "valid-after" time from the consensus header. This timestamp serves as a unique identifier for the consensus during processing and is used to aggregate data based on the UTC date of this timestamp.

For each consensus entry, do the following:

Parse the server descriptor digest from the "r" line, but this is only necessary for statistics based on relay server descriptor contents.
Look for "a" lines and determine whether at least one of them contains an IPv6 address. This indicates the presence of a reachable IPv6 OR address for the relay.
Extract relay flags from the "s" line. If there's no "Running" flag, exclude this consensus entry to focus on running relays. Also, parse any other relay flags from the "s" line that are assigned to the relay.

If a consensus doesn't contain any running relays, we skip it, mainly to eliminate a rare scenario in which only a minority of directory authorities voted on the "Running" flag. In such cases, including such a consensus would distort the average, even if relays were operational.

#### Step 2: Parsing Relay Server Descriptors

Next, we obtain relay server descriptors from CollecTor, and you can refer to the Tor directory protocol, version 3, for details on the descriptor format.

For each server descriptor, parse any of the following components:

Extract the Tor software version from the "platform" line and record the first three dotted numbers from it. If the "platform" line doesn't start with "Tor" followed by a space and a dotted version number, categorize it as "Other." If the platform line is missing, we skip this descriptor, leading to the exclusion of this relay from the count rather than classifying it under "Other." Note that consensus entries also include a "v" line with the Tor software version from the referenced descriptor, but we don't use it because it wasn't present in very old consensuses. However, it should work equally well for recent consensuses.

Examine the "platform" line to identify whether it contains any of the substrings "Linux," "Darwin" (macOS), "BSD," or "Windows." If none of these substrings are present, categorize the platform as "Other." If the platform line is missing, we skip this descriptor, with the same intention of excluding the relay from the count rather than classifying it as "Other."

Check for "or-address" lines and determine whether at least one of them contains an IPv6 address. This indicates that the relay announced an IPv6 address.

Look for the "ipv6-policy" line, if present, and note whether it differs from "reject 1-65535." This indicates whether the relay allows exiting to IPv6 targets. If the line is absent, we assume that the relay does not permit exiting to IPv6 targets.

Compute the SHA-1 digest or determine it from the file name in the case of archived descriptor tarballs.

#### Step 3: Calculating Daily Averages

Match consensus entries with server descriptors based on the SHA-1 digest. Each consensus entry references a single server descriptor, and a server descriptor may be referenced by any number of consensus entries. If more than 0.1% of referenced server descriptors are missing, the consensus is skipped, as missing server descriptors can easily distort the results. However, a small number of missing server descriptors per consensus is acceptable and unavoidable.

Go through all previously processed consensuses, organized by valid-after UTC date. Compute the mean number of running relays, potentially categorized by relay flag, Tor version, platform, or IPv6 capabilities. This is done by summing the counts of all running relays and dividing by the number of consensuses. The result is rounded down to the nearest integer.

Exclude the last day of results if it matches the current UTC date, as these averages may still change throughout the day. Additionally, skip days for which fewer than 12 consensuses are available. The aim is to prevent overrepresentation of a few consensuses during periods when directory authorities struggled to produce a consensus for at least half of the day.

### Estimating Running Relays from VictoriaMetrics

It is possible to quickly estimate the number of running relays from counting
network_fraction by day over 24 hours:

```
sum_over_time(count(network_fraction) by (day))[24h]
```

The `network_fraction` metric of a relay is based on bandwidth observed by the relay and bandwidth measured by the directory authorities as a fraction of the total network weight.

### Estimating Running Bridges

Following our explanation of running relay statistics, we now turn our attention to the running bridges statistics. While the steps are quite similar, there are a few variations in data formats that warrant a separate explanation for these statistics. The descriptions provided here pertain to the following graphs:

Relays and bridges (focusing on the bridges section; details about relays can be found above)
Bridges categorized by IP version

#### Step 1: Parsing Bridge Network Statuses

To start, we obtain bridge network statuses from CollecTor. For more details on the descriptor format, you can refer to the Tor bridge descriptors page.

Parse the bridge authority identifier from the file name and make a note of it. This is particularly relevant during times when more than one bridge authority was operational. In such cases, bridges usually register with a single bridge authority, so averaging the number of running bridges across all statuses for that day would be misleading.

Extract and remember the "published" time from either the file name or the status header. This timestamp serves as a unique identifier for the status during processing, and the UTC date of this timestamp is used later for aggregating by UTC date.

For each status entry, do the following:

Parse the server descriptor digest from the "r" line and remember it.
Extract the relay flag from the "s" line. If there's no "Running" flag, exclude this entry to focus solely on running bridges.

If a status entry contains no running bridges, we skip it. This might occur during a temporary issue with the bridge authority.

#### Step 2: Parsing Bridge Server Descriptors

Now, we acquire bridge server descriptors from CollecTor. As previously mentioned, refer to the Tor bridge descriptors page for details on the descriptor format.

For each server descriptor, parse the following components:

Check for "or-address" lines and determine whether at least one of them contains an IPv6 address. This indicates that the bridge has announced an IPv6 address.
Parse the SHA-1 digest from the "router-digest" line, or determine it from the file name in the case of archived descriptor tarballs.

#### Step 3: Calculating Daily Averages

Match status entries with server descriptors based on the SHA-1 digest. Each status entry references a single server descriptor, and a server descriptor may be referenced by any number of status entries. If more than 0.1% of referenced server descriptors are missing, the status is skipped, as a substantial number of missing server descriptors could easily skew the results. However, a small number of missing server descriptors per status is acceptable and unavoidable.

Compute the arithmetic mean of running bridges by summing the counts of all running bridges and dividing by the number of statuses. The result is rounded down to the nearest integer. It's important to note that this approach does not accurately represent the fact that bridges typically register with a single bridge authority only.

Exclude the last day of results if it matches the current UTC date, as these averages may still change throughout the day. Additionally, skip days for which fewer than 12 statuses are available, aiming to avoid over-representing a few statuses during periods when the bridge directory authority had difficulty producing a status for at least half of the day.

### Estimating Running Bridges from VictoriaMetrics

It is possible to quickly estimate the number of running relays from counting
bridge_bandwidth by day over 24 hours:

```
sum_over_time(count(bridge_bandwidth) by (day))[24h]
```

The `bridge_bandwidth` metric measures the bridge bandwidth in bytes per second.

### Consensus Weight Analysis

In this statistic, we utilize measured bandwidth, often referred to as consensus weight, as the primary metric for relay statistics, rather than relying on absolute relay counts. This approach allows for a more detailed and nuanced evaluation of the network's performance.

#### Step 1: Parsing Consensuses

First, we collect consensuses from CollecTor, and you can find detailed information on the descriptor format in the Tor directory protocol, version 3.

Extract and retain the "valid-after" time from the consensus header. This UTC timestamp is used for aggregating the data based on the UTC date.

Parse the "s" lines in all status entries and exclude entries that lack the "Running" flag. Optionally, we can differentiate relays based on their assigned "Guard" and "Exit" flags.

Parse the (optional) "w" lines in all status entries and calculate the total bandwidth by summing all values indicated by the "Bandwidth=" keyword. If an entry doesn't contain such a value, we skip that entry. If a consensus lacks any bandwidth value, we skip the entire consensus.

#### Step 2: Parsing Votes

Next, we obtain votes from CollecTor, with details on the descriptor format available in the Tor directory protocol, version 3.

Capture and store the "valid-after" time from the vote header, using this UTC timestamp for aggregating based on the UTC date.

Also, parse the "nickname" and "identity" fields from the "dir-source" line. We use the identity for aggregating by authority and the nickname for display purposes.

Parse the "s" lines in all status entries and exclude entries that lack the "Running" flag. Optionally, we can differentiate relays based on their assigned "Guard" and "Exit" flags.

Parse the (optional) "w" lines in all status entries and calculate the total measured bandwidth by summing all values indicated by the "Measured=" keyword. If an entry doesn't contain such a value, we skip that entry. If a vote lacks any measured bandwidth value, we skip the entire vote.

#### Step 3: Calculating Daily Averages

We proceed by reviewing all the consensuses and votes that were previously processed, organized by valid-after UTC date and authority. We set criteria for filtering the data:

If there are fewer than 12 consensuses available for a specific UTC date, we skip consensuses from that date.
If an authority published fewer than 12 votes on a given UTC date, we skip both that date and that authority.
We also exclude the most recent date in the results, as these averages may still change throughout the day.

For the remaining combinations of dates and authorities, we calculate the arithmetic mean of the total measured bandwidth, rounding down to the nearest integer. This method ensures that the data is representative of network performance while accounting for any variations in measurements.

### Consensus Weight Metrics on VictoriaMetrics

Here are a list of metrics published for each relay. Please refer to the [metrics](https://gitlab.torproject.org/tpo/network-health/metrics/descriptorParser/-/blob/main/METRICS.md) for the detailed explanation of each of them.

- network_weight
- network_fraction
- network_guard_weight
- network_guard_fraction
- network_exit_weight
- network_exit_fraction
- network_middle_weight
- network_middle_fraction

There is also a list of network totals published for all relays:

- total_network_weight
- total_network_exit_weight
- total_network_guard_weight
- total_network_middle_weight

## Traffic Statistics

### Advertised Bandwidth

Advertised bandwidth refers to the amount of traffic, both incoming and outgoing, that a relay is configured to handle, as reported by the operator and observed from recent data transfers. This data is self-reported by relays in their server descriptors and is evaluated alongside consensuses.

#### Step 1: Parsing Relay Server Descriptors

Obtain relay server descriptors from CollecTor, referring to the Tor directory protocol, version 3, for details on the descriptor format.

Parse the following information from each server descriptor:
Advertised bandwidth: Extract three values (or two in older descriptors) from the "bandwidth" line. These values represent the average bandwidth, burst bandwidth, and observed bandwidth. The advertised bandwidth is determined as the minimum of these values.
Compute the SHA-1 digest for the server descriptor or determine it from the file name, in the case of archived descriptor tarballs.

#### Step 2: Parsing Consensuses

Obtain consensuses from CollecTor, and you can refer to the Tor directory protocol, version 3, for descriptor format details.

For each consensus, extract the "valid-after" time from the header section.

For each consensus entry, parse the base64-encoded server descriptor digest from the "r" line. This digest is used to match the entry with the advertised bandwidth value from server descriptors.

Also, parse the relay flags from the "s" line. If the "Running" flag is absent, skip this entry. (Consensuses with consensus method 4 or later typically do not list non-running relays, but we still check relay flags in recent consensuses as a precaution.)

Further, parse the "Guard," "Exit," and "BadExit" relay flags from this line. A relay with the "Guard" flag is considered a guard, and a relay with the "Exit" flag (without the "BadExit" flag) is considered an exit relay.

#### Step 3: Computing Daily Averages

The first three graphs, which are Total relay bandwidth, Advertised and consumed bandwidth by relay flags, and Advertised bandwidth by IP version, display daily averages of advertised bandwidth.

To calculate these averages:

Match consensus entries with server descriptors by SHA-1 digest. If at least 0.1% of referenced server descriptors are missing, we exclude the consensus to prevent skewing of results.

For each UTC date, compute the arithmetic mean of advertised bandwidth as the sum of all advertised bandwidth values divided by the number of consensuses. This result is rounded down to the nearest integer.

Categorize the numbers by guard and/or exit relay based on the relay flags in consensus entries that reference a server descriptor.

Skip the last day of results if it matches the current UTC date, as those averages may change throughout the day. Further, skip days with fewer than 12 consensuses to avoid over-representing specific consensuses during periods when the directory authorities may have had difficulty producing consensuses for at least half of the day.

#### Step 4: Computing Ranks and Percentiles

The remaining two graphs, Advertised bandwidth distribution and Advertised bandwidth of n-th fastest relays, present advertised bandwidth ranks or percentiles.

Similar to the previous step:

Match consensus entries with server descriptors by SHA-1 digest. If server descriptors are missing, we skip the consensus entry, even though this may lead to over-representation of available server descriptors in consensuses where most descriptors are missing.

For the Advertised bandwidth distribution graph, calculate the i-th percentile value for each consensus. A non-standard percentile definition, loosely based on the nearest-rank method, is used. The percentile is calculated for each consensus, and the median value for each percentile is computed across all consensuses from a given day.

For the Advertised bandwidth of n-th fastest relays graph, determine the n-th highest advertised bandwidth value for each consensus and calculate the median value over all consensuses from a given day. This analysis includes all running relays and exit relays.

### Computing Advertised Bandwidth On VictoriaMetrics

```
sum(sum_over_time((min(bw_file_desc_bw_avg, bw_file_desc_bw_bur, bw_file_desc_bw_obs_last) by (fingerprint, time)))[24h])*8

```

### Estimating Consumed Bandwidth

Consumed bandwidth, often referred to as bandwidth history, represents the volume of incoming and/or outgoing traffic that a relay asserts to have managed on behalf of clients. This data is self-reported by relays in their extra-info descriptors, and we analyze it in conjunction with consensuses.

#### Step 1: Parsing Extra-Info Descriptors

Obtain extra-info descriptors from CollecTor. You can find details about the descriptor format in the Tor directory protocol, version 3.

Extract the fingerprint from the "extra-info" line, which will be used for deduplicating statistics contained in other extra-info descriptors published by the same relay. It may also assist in attributing statistics to relays with the "Exit" and/or "Guard" flags.

Parse the "write-history," "read-history," "dirreq-write-history," and "dirreq-read-history" lines that contain statistics on consumed bandwidth. The first two histories encompass all bytes written or read by the relay, while the last two include only bytes spent on responding to directory requests. If a statistics interval spans more than one UTC date, we divide the observations across the corresponding UTC dates using a linear distribution. We simplify this by shifting reported statistics intervals to align with 15-minute multiples since midnight. We also discard statistics with intervals that are not multiples of 15 minutes.

#### Step 2: Parsing Consensuses

Acquire consensuses from CollecTor, with reference to the Tor directory protocol, version 3, for format details.

In each consensus, retrieve the "valid-after" time from the header section.

In each consensus entry, parse the base64-encoded relay fingerprint from the "r" line. This fingerprint will be used to match the entry with statistics from extra-info descriptors.

Also, parse the relay flag from the "s" line. If there is no "Running" flag, disregard this entry. (Consensuses with consensus method 4 or later mostly do not list non-running relays, so checking relay flags in recent consensuses is often a precautionary measure.)

Additionally, parse the "Guard," "Exit," and "BadExit" relay flags from this line. A relay with the "Guard" flag is considered a guard, while a relay with the "Exit" flag (without the "BadExit" flag) is considered an exit relay.

#### Step 3: Calculating Daily Totals

The first two graphs, "Total relay bandwidth" and "Advertised and consumed bandwidth by relay flags," illustrate daily totals of all bytes written or read by relays. For both graphs, the sum of all read and written bytes on a given day is divided by 2. However, bandwidth histories are only included for a given day if a relay was listed as running in a consensus at least once on that day. Bandwidth is attributed to guards and/or exits if a relay acted as a guard and/or exit in at least one consensus on that day.

The third graph, "Bandwidth spent on answering directory requests," displays bytes expended by directory authorities and directory mirrors on responding to directory requests. Unlike the first two graphs, all bandwidth histories are included, irrespective of whether a relay was listed as running in a consensus. This graph calculates total read directory and total written directory bytes, instead of averaging the two.

### Computing Consumed Bandwidth On VictoriaMetrics

```
(sum(sum_over_time(write_bandwidth_history{node="relay"}[24h])) + sum(sum_over_time(read_bandwidth_history{node="relay"}[24h])))*8/2
```

