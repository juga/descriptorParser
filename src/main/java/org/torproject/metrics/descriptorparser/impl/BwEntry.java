package org.torproject.metrics.descriptorparser.impl;

// Class to store bandwidth entries by timestamp
public class BwEntry {
  private final long timestamp;
  private Long writeValue;          // Use Long to allow null values
  private Long readValue;           // Use Long to allow null values
  private Long ipv6WriteValue;      // Use Long to allow null values
  private Long ipv6ReadValue;       // Use Long to allow null values
  private Long dirreqWriteValue;    // Use Long to allow null values
  private Long dirreqReadValue;     // Use Long to allow null values

  public BwEntry(long timestamp) {
    this.timestamp = timestamp;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public Long getWriteValue() {
    return writeValue;
  }

  public void setWriteValue(Long writeValue) {
    this.writeValue = writeValue;
  }

  public Long getReadValue() {
    return readValue;
  }

  public void setReadValue(Long readValue) {
    this.readValue = readValue;
  }

  public Long getIpv6WriteValue() {
    return ipv6WriteValue;
  }

  public void setIpv6WriteValue(Long ipv6WriteValue) {
    this.ipv6WriteValue = ipv6WriteValue;
  }

  public Long getIpv6ReadValue() {
    return ipv6ReadValue;
  }

  public void setIpv6ReadValue(Long ipv6ReadValue) {
    this.ipv6ReadValue = ipv6ReadValue;
  }

  public Long getDirreqWriteValue() {
    return dirreqWriteValue;
  }

  public void setDirreqWriteValue(Long dirreqWriteValue) {
    this.dirreqWriteValue = dirreqWriteValue;
  }

  public Long getDirreqReadValue() {
    return dirreqReadValue;
  }

  public void setDirreqReadValue(Long dirreqReadValue) {
    this.dirreqReadValue = dirreqReadValue;
  }
}
