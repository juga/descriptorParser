package org.torproject.metrics.descriptorparser.parsers;

import static java.util.stream.Collectors.toList;

import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.WebServerAccessLog;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.OpenMetricsWriter;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

public class WebStatsParser {
  private static final String INSERT_LOG_SQL
      = "INSERT INTO server_log"
      + " (published, physical_host, virtual_host, digest) VALUES"
      + "(?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String INSERT_LOG_LINE_SQL
      = "INSERT INTO log_line"
      + " (published, log_digest, ip, method, protocol, request,"
      + " size, response, is_valid, digest) VALUES"
      + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final Logger logger = LoggerFactory.getLogger(
      WebStatsParser.class);

  private static OpenMetricsWriter opWriter = new OpenMetricsWriter();
  private static CollectorRegistry registry = new CollectorRegistry();

  private static Counter requestUrlCounter = Counter.build()
      .name("http_requests_total")
      .help("Total http requests")
      .labelNames("request", "method",
          "protocol", "response").register(registry);

  /**
   * Read votes from disk and add data to the DB.
   */
  public void run(String path, Connection conn) throws Exception {

    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(path))) {
      if (descriptor instanceof WebServerAccessLog) {
        WebServerAccessLog desc;
        desc = (WebServerAccessLog) descriptor;
        DescriptorUtils descUtils = new DescriptorUtils();
        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());
        this.addLog(desc, conn, digest);
        List<? extends WebServerAccessLog.Line> lines
            = desc.logLines().collect(toList());
        int index = 0;
        for (WebServerAccessLog.Line line: lines) {
          String lineFormatted = line.toLogString() + desc.getPhysicalHost()
              + desc.getVirtualHost() + String.valueOf(index)
              + desc.getLogDate().toString();
          index++;
          String lineDigest = descUtils.calculateDigestSha256Base64(
              lineFormatted.getBytes());
          this.addLogLine(line, conn, lineDigest, digest);
        }

      } else {
        logger.warn("Invalid descriptor. Continuing...");
      }
      opWriter.pushToGateway(registry);
    }

  }

  private void addLogLine(WebServerAccessLog.Line line, Connection conn,
      String lineDigest, String logDigest) {
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_LOG_LINE_SQL);
    ) {
      LocalDateTime logDate = line.getDate().atStartOfDay();
      ZonedDateTime zdt = ZonedDateTime.of(logDate, ZoneId.systemDefault());
      long published = zdt.toInstant().toEpochMilli();
      preparedStatement.setTimestamp(1, new Timestamp(published));
      preparedStatement.setString(2, logDigest);
      preparedStatement.setString(3, line.getIp());
      preparedStatement.setString(4, line.getMethod().toString());
      preparedStatement.setString(5, line.getProtocol());
      preparedStatement.setString(6, line.getRequest());
      if (line.getSize().isPresent()) {
        preparedStatement.setInt(7, line.getSize().get());
      } else {
        preparedStatement.setInt(7, 0);
      }
      preparedStatement.setInt(8, line.getResponse());
      preparedStatement.setBoolean(9, line.isValid());
      preparedStatement.setString(10, lineDigest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

    try {
      requestUrlCounter.labels(
          line.getRequest(),
          line.getMethod().toString(),
          line.getProtocol(),
          String.valueOf(line.getResponse())).inc();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addLog(WebServerAccessLog desc, Connection conn, String digest) {
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_LOG_SQL);
    ) {
      LocalDateTime logDate = desc.getLogDate().atStartOfDay();
      ZonedDateTime zdt = ZonedDateTime.of(logDate, ZoneId.systemDefault());
      long published = zdt.toInstant().toEpochMilli();
      preparedStatement.setTimestamp(1, new Timestamp(published));
      preparedStatement.setString(2, desc.getPhysicalHost());
      preparedStatement.setString(3, desc.getVirtualHost());
      preparedStatement.setString(4, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
