package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class WebStatsParserTest {

  @Test()
  public void testWebStatsParserDbUploader() throws Exception {
    WebStatsParser wsp = new WebStatsParser();
    String webStatsPath =
        "src/test/resources/torproject.org_web-"
        + "fsn-02.torproject.org_access.log_20230930";
    String confFile = "src/test/resources/config.properties.test";
    String webStatsDigest
        = "Qspn7CukT3td8TrzSzWBb/6uC44hsOacrvJFqONhMxc";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    wsp.run(webStatsPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_log WHERE digest = '"
        + webStatsDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), webStatsDigest);
      } else {
        fail("Descriptor not found");
      }
    }

    String logDigest = "3cjuNFzw8Lu3eaZVy/FBKJQvrXGXDQxKDic2J38adx0";
    PreparedStatement preparedLogStatement = conn.prepareStatement(
        "SELECT * FROM log_line WHERE digest = '"
        + logDigest + "'");

    try (ResultSet rsLog = preparedLogStatement.executeQuery()) {
      if (rsLog.next()) {
        assertEquals(rsLog.getString("digest"), logDigest);
      } else {
        fail("Line not found");
      }
    }
  }
}
