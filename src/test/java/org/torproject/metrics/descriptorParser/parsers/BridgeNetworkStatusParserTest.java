package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BridgeNetworkStatusParserTest {

  @Test()
  public void testBridgeNetworkStatusParserDbUploader() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    String bridgeNetworkStatusDigest =
        "EoCh76XYZkmH2sbQoRNIgz7hMbJAvFR6WXsbf8LGe2g";

    String serge = "BA44A889E64B93FAA2B114E02C2A279A8555C533";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bridge_network_status WHERE digest = '"
        + bridgeNetworkStatusDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bridgeNetworkStatusDigest);
        assertEquals(rs.getString("fingerprint"), serge);
      } else {
        fail("Bridges network status not found");
      }
    }

    String fingerprint = "005FD4D7DECBB250055B861579E6FDC79AD17BEE";
    PreparedStatement preparedEntryStatement = conn.prepareStatement(
        "SELECT * FROM bridge_network_status_entry WHERE network_status = '"
        + bridgeNetworkStatusDigest + "' AND fingerprint='"
        + fingerprint + "' ORDER BY published DESC LIMIT 1;");

    try (ResultSet rs = preparedEntryStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("network_status"), bridgeNetworkStatusDigest);
        assertEquals(rs.getString("fingerprint"), fingerprint);
        assertEquals(rs.getInt("bandwidth"), 5057);
      } else {
        fail("Bridge status not found");
      }
    }

  }


}
