package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ServerDescriptorParserTest {

  @Test()
  public void testServerDescriptorParserDbUploader() throws Exception {
    String confFile = "src/test/resources/config.properties.test";
    String serverDigest = "4dc9c214092201f889e5d7654e602a7489ec35af";
    String fingerprint = "22F74E176F803499D4F80D9CE7D325883A8C0E45";
    PsqlConnector psqlConn = new PsqlConnector();
    Connection conn = null;
    conn = psqlConn.connect(confFile);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_descriptor WHERE digest_sha1_hex = '"
        + serverDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest_sha1_hex"), serverDigest);
        assertEquals(rs.getString("fingerprint"),
            fingerprint);
      } else {
        fail("Descriptor not found");
      }
    }

  }

}
