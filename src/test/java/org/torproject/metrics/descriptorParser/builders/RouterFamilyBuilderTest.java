package org.torproject.metrics.descriptorparser.builders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.parsers.ServerDescriptorParser;
import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RouterFamilyBuilderTest {

  /**
   * Insert server descriptor data into the database before doing tests.
   */
  @Before()
  public void insertServerDescriptorsAndBuildStatuses() throws Exception {
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    String confFile = "src/test/resources/config.properties.test";
    conn = psqlConn.connect(confFile);

    String serverPath =
        "src/test/resources/2022-08-31-server-descriptors";
    ServerDescriptorParser sp = new ServerDescriptorParser();
    sp.run(serverPath, conn);
    long millis = 1661947500000L;
    RouterFamilyBuilder rb = new RouterFamilyBuilder();
    rb.build(conn, millis);

  }

  @Test()
  public void testRouterFamilyBuilder() throws Exception {
    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    String confFile = "src/test/resources/config.properties.test";
    conn = psqlConn.connect(confFile);

    String digest = "asEEWRekGZpmkT4RDAflFbkfcTnjaDWxtcb47jpZ9J4";
    String family = "A4E0F37BAC3E38F503B42BC19B3C0BFADF8248DD "
        + "E42693F28ECCB5AFBC290371DF275C6FD0657F78";

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM server_families WHERE digest='" + digest + "';");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), digest);
        assertEquals(rs.getString("effective_family"), family);
      } else {
        fail("No families found");
      }
    }

  }

}
