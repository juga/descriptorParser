#!/bin/bash

PARSER_HOME="/srv/parser.torproject.org/parser"
DATA_HOME="$PARSER_HOME/descriptors"
ARCHIVE_URL="https://collector.torproject.org/archive"
LOG_FILE="$PARSER_HOME/logs/downloads.log"
PROCESSED_LOG="$PARSER_HOME/logs/processed.log"

paths=(
  "relay-descriptors/bandwidths"
  "bridgedb-metrics"
  "bridge-descriptors/statuses"
  "bridge-pool-assignments"
  "bridgestrap"
  "relay-descriptors/consensuses"
  "relay-descriptors/microdescs/consensus-microdesc"
  "relay-descriptors/microdescs/micro"
  "relay-descriptors/votes"
  "exit-lists"
  "bridge-descriptors/extra-infos"
  "relay-descriptors/extra-infos"
  "bridge-descriptors/server-descriptors"
  "relay-descriptors/server-descriptors"
  "webstats"
)

archive_paths=(
  "relay-descriptors/bandwidths"
  "relay-descriptors/consensuses"
  "relay-descriptors/microdescs"
  "relay-descriptors/extra-infos"
  "relay-descriptors/server-descriptors"
  "relay-descriptors/votes"
  "bridge-descriptors/server-descriptors"
  "bridge-descriptors/extra-infos"
  "bridge-descriptors/statuses"
  "bridge-pool-assignments"
  "bridgedb-metrics"
  "bridgestrap"
  "exit-lists"
)
mkdir -p $PARSER_HOME/logs

# Check if the year and month are passed as command-line arguments
if [ "$#" -ne 2 ]; then
    # Process recent documents
    echo "No year, month combination was provided, processing recent documents instead."
    for p in ${paths[@]}
    do
      if [ -d "$DATA_HOME/${p}" ]; then rm -Rf $DATA_HOME/$p; fi
      mkdir -p $DATA_HOME/${p}
      urls=$(curl https://collector.torproject.org/recent/$p/ | grep -oP "(HREF|href)=\"\K.+?(?=\")")
      urls_array=($urls)
      cd $DATA_HOME/$p
      for u in ${urls_array[@]:5}
      do
        echo $u
        download_url=https://collector.torproject.org/recent/$p/$u
        log_file=$PARSER_HOME/logs/downloads.log
        if ! grep -q "$download_url" "$log_file"; then
          status=$(wget --server-response ${download_url} 2>&1 | awk '/^  HTTP/{print $2}')
          if [ "$status" = "200" ]; then
            echo "$download_url" >> $LOG_FILE
          fi
        fi
      done
    done
    cd $PARSER_HOME
else
  # Make sure to specify year and month in the format YYYY MM, Ex: 2024 01
  year=$1
  month=$2
  specified_month="${year}-${month}"

  # Find last processed day and set the day to process
  last_processed_day=$(cat "$PROCESSED_LOG" 2>/dev/null || echo "")

  # Calculate the first day of the specified month
  first_day_of_month="$year-$month-01"

  if [ -z "$last_processed_day" ]; then
    # Assign the last day of the passed month if the file is empty
    last_day_of_month=$(date -d "$first_day_of_month +1 month -1 day" +"%Y-%m-%d")
    last_processed_day=$last_day_of_month
    echo "Log file is empty, starting from the last day of the specified month"
  else
    if [ "$last_processed_day" == "$first_day_of_month" ]; then
      # If the last processed day is the first day of the month, continue from the previous month
      last_day_of_previous_month=$(date -d "$first_day_of_month -1 day" +"%Y-%m-%d")
      day_to_process=$last_day_of_previous_month
      echo "Continuing from the last day of the previous month: $day_to_process"
    elif [ "$last_processed_day" == "$last_day_of_month" ]; then
      day_to_process="$last_day_of_month"
      echo "Continuing from the last day of the specified month"
    else
      # To process the day after the last processed day
      day_to_process=$(date -d "$last_processed_day + 1 day" +"%Y-%m-%d")
      echo "Last processed day: $last_processed_day"
      echo "Day to process: $day_to_process"
    fi
  fi

  # Processing archives
  for p in "${archive_paths[@]}"; do
    # Extract the part of the path after the last slash
    q="${p##*/}"
    archive="$ARCHIVE_URL/$q-$specified_month.tar.xz"
    archive_file="$DATA_HOME/archives/$q-$specified_month.tar.xz"
    folder="$p"

    if [[ "$p" == *"bridge-descriptors"* ]]; then
      archive="$ARCHIVE_URL/$p/bridge-$q-$specified_month.tar.xz"
      archive_file="$DATA_HOME/archives/bridge-$q-$specified_month.tar.xz"
    fi

    if [[ "$p" == *"exit-lists"* ]]; then
        archive="$ARCHIVE_URL/$p/exit-list-$specified_month.tar.xz"
        archive_file="$DATA_HOME/archives/exit-list-$specified_month.tar.xz"
        folder="exit-lists"
    fi

    mkdir -p "$DATA_HOME/archives"
    if [ ! -f "$archive_file" ]; then
      wget -O "$archive_file" "$archive"
    fi
    tar -xf "$archive_file" -C "$DATA_HOME/archives"

    # Remove the .tar.xz extension to get the filename
    filepath="${archive_file%.tar.xz}"
    move_folder="$DATA_HOME/$folder"

    # Move files to top directory and record the last processed day
    find "$filepath" -type f -newermt "$day_to_process" ! -newermt "$day_to_process + 1 day" -exec mv {} "$move_folder" \;
    find "$filepath" -type d -exec rm -rf {} + ;
  done

  echo "$day_to_process" > $PROCESSED_LOG

  # Clean old entries from downloads.log
  find "$PARSER_HOME/logs" -type f -name 'downloads.log' -exec awk -v last_week=$(date --date="7 days ago" +%Y-%m-%d) '
  {
      split($0, a, "/");
      split(a[5], b, "-");
      file_date=b[1] "-" b[2] "-" b[3];
      if (file_date >= last_week)
          print $0
  }' {} \; > "$LOG_FILE.tmp" && mv "$LOG_FILE.tmp" "$LOG_FILE"
fi

cd $PARSER_HOME
java -XX:+UseShenandoahGC -Xms32m -XX:+UnlockExperimentalVMOptions -XX:ShenandoahUncommitDelay=1000 -XX:ShenandoahGuaranteedGCInterval=10000 -Xmx12g -DLOGBASE=logs/parser -jar parser.jar


