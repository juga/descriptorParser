# Metrics exposed to VictoriaMetrics

## From bandwidth files

bw
The scaled bandwidth in kilobytes per second calculated as explained at [bandwidth-file-spec](https://spec.torproject.org/bandwidth-file-spec/scaling-bandwidths.html#torflow-aggregation).

bw_mean
The measured mean bandwidth in bytes per second.

bw_median
The measured bandwidth median in bytes per second.

consensus_bandwidth
The consensus bandwidth in bytes per second.

bw_file_desc_bw_avg
The descriptor average bandwidth in bytes per second.

bw_file_desc_bw_bur
The descriptor observed bandwidth burst in bytes per second.

bw_file_desc_bw_obs_last
The last descriptor observed bandwidth in bytes per second.

bw_file_desc_bw_obs_mean
The descriptor observed bandwidth mean in bytes per second.

error_circ
The number of times that the bandwidth measurements failed because of circuit
failures.

error_destination
The number of times that the bandwidth measurements failed because the web
server was not available

error_misc
The number of times that the bandwidth measurements failed because of other
reasons.

error_second_relay
The number of times that the bandwidth measurements for this relay failed
because sbws could not find a second relay for the test circuit.

error_stream
The number of times that the bandwidth measurements failed because of stream
failures.

r_strm
The stream ratio calculated as explained at [bandwidth-file-spec](https://spec.torproject.org/bandwidth-file-spec/scaling-bandwidths.html#torflow-aggregation).

r_strm_filt
The filtered stream ratio of this relay calculated as explained at [bandwidth-file-spec](https://spec.torproject.org/bandwidth-file-spec/scaling-bandwidths.html#torflow-aggregation).

## From bridge network statuses

desc_dirreq_v3_status
Estimated number of intervals when the node was listed as running in the network
status published by either the directory authorities or bridge authority.

bridge_bandwidth
The measured bandwidth in bytes per second.

## From consensuses

relay_bandwidth
The measured bandwidth in bytes per second

desc_status
Estimated number of intervals when the node was listed as running in the network
status published by either the directory authorities or bridge authority.

network_weight
Metric of a relay that is based on bandwidth observed by the relay and bandwidth
measured by the directory authorities.

network_fraction
Metric of a relay that is based on bandwidth observed by the relay and bandwidth
measured by the directory authorities as a fraction of the total network weight.

total_network_weight
Metric based on bandwidth observed by relays and bandwidth measured by the
directory authorities.

network_guard_weight
Metric of a guard relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities.

network_guard_fraction
Metric of a guard relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities as a fraction of the total
network weight.

total_network_guard_weight
Metric based on bandwidth observed by guard relays and bandwidth measured by the
directory authorities.

network_middle_weight
Metric of a middle relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities.

network_middle_fraction
Metric of a middle relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities as a fraction of the total
network weight.

total_network_middle_weight
Metric based on bandwidth observed by middle relays and bandwidth measured by
the directory authorities.

network_exit_weight
Metric of a exit relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities.

network_exit_fraction
Metric of a exit relay that is based on bandwidth observed by the relay and
bandwidth measured by the directory authorities as a fraction of the total
network weight.

total_network_exit_weight
Metric based on bandwidth observed by exit relays and bandwidth measured by the
directory authorities.

# From extra-info descriptors

read_bandwidth_history
How much bandwidth the OR has used recently. Usage is divided into intervals of
NSEC seconds.

write_bandwidth_history
How much bandwidth the OR has used recently. Usage is divided into intervals of
NSEC seconds.

ipv6_read_bandwidth_history
How much bandwidth the OR has used recently on ipv6 connections. Usage is
divided into intervals of NSEC seconds.

ipv6_write_bandwidth_history
How much bandwidth the OR has used recently on ipv6 connections. Usage is
divided into intervals of NSEC seconds.

dirreq_read_bandwidth_history
Declare how much bandwidth the OR has spent on answering directory requests.
Usage is divided into intervals of NSEC seconds.

dirreq_write_bandwidth_history
Declare how much bandwidth the OR has spent on answering directory requests.
Usage is divided into intervals of NSEC seconds.

desc_relay_dirreq_v3_responses
Estimated number of v3 network status consensus requests that the node responded to.

desc_bridge_dirreq_v3_responses
Estimated number of v3 network status consensus requests that the node responded to.

# From server descriptors

desc_bw_avg
Estimated average bandwidth the the OR is willing to sustain over long period.
In bytes per second.

desc_bw_bur
Estimated average bandwidth the the OR is willing to sustain over short
intervals. In bytes per second.

desc_bw_obs
Estimated bandwidth capacity this relay can handle. In bytes per second.

desc_bw_adv
Estimated advertised bandwidth this relay can handle. In bytes per second.

# From web logs
http_requests_total
Total http requests
